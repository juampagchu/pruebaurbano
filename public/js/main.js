window.onload = function() {
	ActualizarListadoDeGrupos();
	ListarClientes();
}

/* CLIENTES */

function CrearCliente() {
	$("#listado-cliente").attr("class","col-6");
	$("#contenedor-frm-cliente").attr("class","col-6");
	$.ajax({
  		url: _TB_HOST+"ajax/crearCliente.php"
	}).done(function(data){
	    if (data && data!="") {
			$("#contenedor-frm-cliente").html(data);
	    }
  	});
}

function EditarCliente(id) {
	$("#listado-cliente").attr("class","col-6");
	$("#contenedor-frm-cliente").attr("class","col-6");
	$.ajax({
  		url: _TB_HOST+"ajax/editarCliente.php",
  		data:{
  			id:id
  		},
  		success: function(result){
  			if (result && result!="") {
				$("#contenedor-frm-cliente").html(result);
	    	}
  		},
  		error: function(result){
  			CancelarCrearCliente();
  			swal({
		        title: "Error",
		        text: "Ocurrió un error al querer editar este cliente",
		        type: "error"
	    	});
  		}
	});
}

function EliminarCliente(id) {
	swal({
        title: "Eliminar cliente",
        text: "¿Estas seguro de eliminar este cliente?",
        type: "warning",
        showCancelButton: true
    }, function (onConfirm) {
        if(onConfirm){ 
			$.ajax({
		  		url: _TB_HOST+"ajax/eliminarCliente.php",
		  		data:{
		  			id:id
		  		},
		  		success: function(result){
		  			if (result && result.ok) {
						ListarClientes();
				    	CancelarCrearCliente();
				    	swal({
					        title: "Muy bien",
					        text: "Sus datos el cliente fue borrado exitosamente.",
					        type: "success"
					    });
			    	}
		  		},
		  		error: function(result){
		  			CancelarCrearCliente();
		  			swal({
				        title: "Error",
				        text: "Ocurrió un error al borrar este cliente",
				        type: "error"
			    	});
		  		}
			});
        }
    });
}


function ListarClientes(){
	$.ajax({
  		url: _TB_HOST+"ajax/listarClientes.php",
  		data:{
  			busqueda:$("#filtro-buscar").val(),
			grupo_id:$("#filtro-grupo").val()
  		}
	}).done(function(data){
	    if (data && data!="") {
			$("#conten-clientes").html(data);
	    }
  	});
}

function ActualizarListadoDeGrupos(){
	$("#filtro-grupo").html('<option value="">Grupos</option>');
	$.ajax({
  		url: _TB_HOST+"ajax/listadoDeGrupos.php",
  		success: function(result){
  			var optionsHtml = '<option value="">Grupos</option>';
  			if (result && result[0]) {
				for(i in result){
					optionsHtml += '<option value="'+result[i]["id"]+'">'+result[i]["nombre"]+'</option>';
				}
	    	}
	    	$("#filtro-grupo").html(optionsHtml);
  		}
	});
}

function CancelarCrearCliente() {
	$("#listado-cliente").attr("class","col-12");
	$("#contenedor-frm-cliente").attr("class","d-none");
}

function GuardarCliente(){
	var guardar = true;
	if($("#nombre").val().trim() == 0){ 	
		guardar= MostrarError("nombre");
	}else{
		OcultarError("nombre");
	}
	
	if($("#apellido").val().trim() == 0){ 	
		guardar= MostrarError("apellido");
	}else{
		OcultarError("apellido");
	}
	if($("#email").val().trim() == 0){ 
		$("#email").parent().find("span").html("Revisa esté campo");
		guardar= MostrarError("email");
	}else{
		if(!ValidarCorreo($("#email").val())){
			$("#email").parent().find("span").html($("#email").val()+" no es un email valido.");
			guardar= MostrarError("email");
		}else{
			OcultarError("email");
		}
	}
	if(guardar){
		var clienteReg = {
  			nombre:$("#nombre").val(),
			apellido:$("#apellido").val(),
			email:$("#email").val(),
			grupo_id:$("#grupo_id").val(),
			observaciones:$("#observaciones").val()
  		};
  		if($("#estado") && $("#estado").length > 0){
  			clienteReg["estado"] = $("#estado").val();
  		}
  		if($("#idCliente") && $("#idCliente").length > 0){
  			clienteReg["id"] = $("#idCliente").val();
  		}
		$.ajax({
	  		url: _TB_HOST+"ajax/guardarCliente.php",
	  		method: "POST",
	  		data:clienteReg,
	  		success: function(result){
	  			if(result && result.id){
			    	ListarClientes();
			    	CancelarCrearCliente();
			    	swal({
				        title: "Muy bien",
				        text: "Sus datos fueron guardados exitosamente.",
				        type: "success"
				    });
	  			}
	  		},
	  		error: function(result){
	  			if(result && result.responseJSON && result.responseJSON.error){
	  				if(result.responseJSON.error["nombre"]){
	  					MostrarError("nombre");
	  				}
					if(result.responseJSON.error["apellido"]){
						MostrarError("apellido");
					}
					if(result.responseJSON.error["email"]){
						MostrarError("email");
					}
	  			}
	  			swal({
			        title: "Error",
			        text: "Ocurrió un error al guardar sus datos",
			        type: "error"
			    });
	  		}
		});
	}
}

/* CLIENTES */

/* GRUPO DE CLIENTES */

function CrearGrupoCliente() {
	$("#listado-grupocliente").attr("class","col-6");
	$("#contenedor-frm-grupocliente").attr("class","col-6");
	$.ajax({
  		url: _TB_HOST+"ajax/crearGrupoCliente.php"
	}).done(function(data){
	    if (data && data!="") {
			$("#contenedor-frm-grupocliente").html(data);
	    }
  	});
}

function EditarGrupoCliente(id) {
	$("#listado-grupocliente").attr("class","col-6");
	$("#contenedor-frm-grupocliente").attr("class","col-6");
	$.ajax({
  		url: _TB_HOST+"ajax/editarGrupoCliente.php",
  		data:{
  			id:id
  		},
  		success: function(result){
  			if (result && result!="") {
				$("#contenedor-frm-grupocliente").html(result);
	    	}
  		},
  		error: function(result){
  			CancelarCrearGrupoCliente();
  			swal({
		        title: "Error",
		        text: "Ocurrió un error al querer editar este grupo de clientes",
		        type: "error"
	    	});
  		}
	});
}

function EliminarGrupoCliente(id) {
	swal({
        title: "Eliminar grupo de clientes",
        text: "¿Estas seguro de eliminar este grupo de clientes?",
        type: "warning",
        showCancelButton: true
    }, function (onConfirm) {
        if(onConfirm){ 
			$.ajax({
		  		url: _TB_HOST+"ajax/eliminarGrupoCliente.php",
		  		data:{
		  			id:id
		  		},
		  		success: function(result){
		  			if (result && result.ok) {
						ListarGrupoClientes();
				    	CancelarCrearGrupoCliente();
				    	swal({
					        title: "Muy bien",
					        text: "Sus datos el cliente fue borrado exitosamente.",
					        type: "success"
					    });
			    	}
		  		},
		  		error: function(result){
		  			CancelarCrearGrupoCliente();
		  			swal({
				        title: "Error",
				        text: "Ocurrió un error al borrar este cliente",
				        type: "error"
			    	});
		  		}
			});
        }
    });
}


function ListarGrupoClientes(){
	$.ajax({
  		url: _TB_HOST+"ajax/listarGrupoClientes.php",
  		data:{
  			busqueda:$("#filtro-buscar-grupo").val()
  		}
	}).done(function(data){
	    if (data && data!="") {
			$("#conten-grupoclientes").html(data);
	    }
  	});
}

function CancelarCrearGrupoCliente() {
	$("#listado-grupocliente").attr("class","col-12");
	$("#contenedor-frm-grupocliente").attr("class","d-none");
}

function GuardarGrupoCliente(){
	var guardar = true;
	if($("#nombre").val().trim() == 0){ 	
		guardar= MostrarError("nombre");
	}else{
		OcultarError("nombre");
	}
	if(guardar){
		var GrupoclienteReg = {
  			nombre:$("#nombre").val()
  		};
  		if($("#estado") && $("#estado").length > 0){
  			GrupoclienteReg["estado"] = $("#estado").val();
  		}
  		if($("#id") && $("#id").length > 0){
  			GrupoclienteReg["id"] = $("#id").val();
  		}
		$.ajax({
	  		url: _TB_HOST+"ajax/guardarGrupoCliente.php",
	  		method: "POST",
	  		data:GrupoclienteReg,
	  		success: function(result){
	  			if(result && result.id){
			    	ActualizarListadoDeGrupos();
			    	ListarGrupoClientes();
			    	CancelarCrearGrupoCliente()
			    	swal({
				        title: "Muy bien",
				        text: "Sus datos fueron guardados exitosamente.",
				        type: "success"
				    });
	  			}
	  		},
	  		error: function(result){
	  			if(result && result.responseJSON && result.responseJSON.error){
	  				if(result.responseJSON.error["nombre"]){
	  					MostrarError("nombre");
	  				}
	  			}
	  			swal({
			        title: "Error",
			        text: "Ocurrió un error al guardar sus datos",
			        type: "error"
			    });
	  		}
		});
	}
}

/* GRUPO DE CLIENTES */

/* GENERICAS */

function MostrarError(idEle){
	$("#"+idEle).addClass("is-invalid");
	$("#"+idEle).parent().find("span").removeClass("d-none");
	return false;
}

function OcultarError(idEle){
	$("#"+idEle).removeClass("is-invalid");
	$("#"+idEle).parent().find("span").addClass("d-none");
	return true;
}

function ValidarCorreo(email){
	var formato = /^([\w-\.\+])+@([\w-]+\.)+([a-z]){2,4}$/;
	var comparacion = formato.test(email);
	return comparacion;
}

function MostrarSeccion(id){
	if(id == "clientes"){
		$("#seccion-gclientes").hide();
		$("#seccion-clientes").show();
		ListarClientes();
	}else{
		$("#seccion-gclientes").show();
		$("#seccion-clientes").hide();
		ListarGrupoClientes();
	}
}

function CrearGrupoPersonasForce(){
	CancelarCrearCliente();
	$("#seccion-gclientes").show();
	$("#seccion-clientes").hide();
	ListarGrupoClientes();
	CrearGrupoCliente();
}

/* GENERICAS */