<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo(_DESCRIPTION); ?>">
    <meta name="author" content="<?php echo(_AUTHOR); ?>">
    <title><?php echo(_APPNAME); ?></title>
    <link href="<?php echo(_TB_HOSTCSS);?>bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo(_TB_HOSTCSS);?>sweetalert.css" rel="stylesheet">
</head>
<body>
    <header class="navbar navbar-expand flex-column flex-md-row border-bottom">
  		<div class="navbar-nav-scroll">
    		<ul class="navbar-nav bd-navbar-nav flex-row">
      			<li class="nav-item">
        			<a class="nav-link " onclick="MostrarSeccion('clientes')" href="#">Clientes</a>
      			</li>
      			<li class="nav-item">
        			<a class="nav-link" onclick="MostrarSeccion('grupos')" href="#">Grupos de clientes</a>
      			</li>      
    		</ul>
  		</div>
	</header>