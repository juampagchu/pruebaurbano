<div class="container-fluid pt-5" id="seccion-gclientes" style="display:none;">
  <div class="row">
    <div class="col-12" id="listado-grupocliente">
      <div class="card">
        <div class="card-body">
          <h3 class="card-title">Grupos de Clientes</h3>
          <p class="card-text">Listado de Grupos de clientes.</p>
          <button class="btn btn-primary" onclick="CrearGrupoCliente()">Crear nuevo</button>
        </div>
        <hr>
        <div class="card-body">
            <div class="row">
              <div class="col-10">
                <input type="text" class="form-control" placeholder="Nombre, Apellido o Email" id="filtro-buscar-grupo">
              </div>
              <div class="col-2">
                <button type="button" onclick="ListarClientes()" class="btn btn-block btn-info btn-xs">Buscar</button>
              </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">Nombre</th>
                  <th scope="col">Acciónes</th>
                </tr>
              </thead>
              <tbody id="conten-grupoclientes">
              </tbody>
            </table>
        </div>
      </div>
    </div>
    <div class="d-none" id="contenedor-frm-grupocliente"></div>
  </div>
</div>

<div class="container-fluid pt-5" id="seccion-clientes">
  <div class="row">
    <div class="col-12" id="listado-cliente">
      <div class="card">
        <div class="card-body">
          <h3 class="card-title">Clientes</h3>
          <p class="card-text">Listado de clientes.</p>
          <button class="btn btn-primary" onclick="CrearCliente()">Crear nuevo</button>
        </div>
        <hr>
        <div class="card-body">
            <div class="row">
              <div class="col-6">
                <input type="text" class="form-control" placeholder="Nombre, Apellido o Email" id="filtro-buscar">
              </div>
              <div class="col-4">
                <select class="form-control" id="filtro-grupo">
                  <option value="">Grupos</option>
                </select>
              </div>
              <div class="col-2">
                <button type="button" onclick="ListarClientes()" class="btn btn-block btn-info btn-xs">Buscar</button>
              </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">Nombre</th>
                  <th scope="col">Apellido</th>
                  <th scope="col">Email</th>
                  <th scope="col">Grupo de cliente</th>
                  <th scope="col">Acciónes</th>
                </tr>
              </thead>
              <tbody id="conten-clientes">
              </tbody>
            </table>
        </div>
      </div>
    </div>
    <div class="d-none" id="contenedor-frm-cliente"></div>
  </div>
</div>