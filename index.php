<?php
	// 1 para reportar errores 0 para no reportar
	define('_TB_DEBUG', 1);

	
	// Directorio raiz del proyecto
	define('_TB_PATHBASE', dirname(__FILE__)."/");
	
	// Extencion standart en mi caso php
	define('_TB_EXT', ".php");

	// Directorio de la vista
	define('_TB_PATHVIEWS', _TB_PATHBASE."views/");

	// Guardo la URL del sistema verificando si viene con SSL o no.
	if (!empty($_SERVER['HTTPS']) && ('on' == $_SERVER['HTTPS'])) { 
		$protocolo = 'https://'; 
	} else { 
		$protocolo = 'http://'; 
	}
	define('_TB_HOST', $protocolo.$_SERVER["HTTP_HOST"]."/");
	define('_TB_HOSTCSS', _TB_HOST."public/css/");
	define('_TB_HOSTJS', _TB_HOST."public/js/");


	// Verificacion de manejo de erorres
	if(_TB_DEBUG){
		error_reporting(E_ALL);
		ini_set("display_errors", "1");
		ini_set("short_open_tag", "1");
		ini_set("allow_url_fopen", 1);
		ini_set("auto_detect_line_endings", true);
	}else{
		error_reporting(0);
	}

	// Cargo las configuraciónes
	require_once 'configs.php';
	// Cargo las herramientas generales
	require_once 'tools.php';

	// Muestro la vista
	require_once _TB_PATHVIEWS.'head.php';
	require_once _TB_PATHVIEWS.'home.php';
	require_once _TB_PATHVIEWS.'footer.php';
?>
