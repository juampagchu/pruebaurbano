<?php
	function ResponseError($response){
		header('X-PHP-Response-Code: 400', true, 400);
		if(is_array($response)){
			header('Content-Type: application/json');
			echo(json_encode(array("error"=>$response)));
		}else if(is_string($response)){
			echo(json_encode(array("error"=>$response)));
		}
	}

	function ResponseOk($response){
		if(is_array($response)){
			header('Content-Type: application/json');
			echo(json_encode($response));
		}else if(is_string($response)){
			echo($response);
		}
	}

	function WriteLog($text) {
		if(!empty($text)){
			umask(0);
			$dirLogs = dirname(__FILE__)."/"."logs/";
			$mes = Date('Y-m');
			$dia = Date('Y-m-d');
			$dir = $dirLogs.$mes;
			$archivo = $dir.'/'.$dia.'.log';
			// Si no existe la carpeta la creo
			if (!file_exists($dir)) { mkdir($dir,0777); }
			if (!file_exists($dirLogs)) { mkdir($dirLogs,0777); }		
			$linea = '['.Date('Y-m-d H:i:s').'] - ';
			$linea .= trim($text).PHP_EOL;
			return file_put_contents($archivo, $linea, FILE_APPEND);
		}
	}
?>
