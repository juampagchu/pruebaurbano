<?php
/**
* cClientes.php 
*/
// Manejo de base de datos
require_once '../db.php';

class cClientes extends cDb {
	static private $tablaClientes = NULL;
	static private $tablaClientesGrupo = NULL;
		
	public function __construct(){
		self::$tablaClientes = _NDB_clientes;
		self::$tablaClientesGrupo = _NDB_grupos_clientes;
		$this->conectar();
	}
	
	// Obtiene todos los registros o bien los filtra
	public function GetAll($busqueda = "", $grupoId=null){
		$sSql = "SELECT c.*,cg.nombre AS nombre_grupo FROM ".self::$tablaClientes." as c LEFT JOIN ".self::$tablaClientesGrupo." as cg ON c.grupo_id = cg.id WHERE 1 ";
		if(!empty($busqueda)){
			$busqueda = strtolower($busqueda);
			$sSql .= " AND ( LOWER(c.nombre) LIKE '%".$busqueda."%' OR  LOWER(c.apellido) LIKE '%".$busqueda."%' OR LOWER(c.email) LIKE '%".$busqueda."%' ) ";
		}
		if(!empty($grupoId)){
			$sSql .= " AND c.grupo_id = '".$grupoId."' ";
		}
		$resp = $this->oDb->getArray($sSql);
		if(!empty($resp)){
			return $resp;
		}else{
			return false;
		}
	}
	
	// Obtiene un registro por ID
	public function Get($nId = false){
		if(!$nId){  WriteLog("El id no es un número"); return false;  }
		$sSql = "SELECT * FROM ".self::$tablaClientes." WHERE id = '".$nId."'";
		$resp = $this->oDb->getArray($sSql);
		if(!empty($resp) && isset($resp[0])){
			return $resp[0];
		}else{
			return false;
		}
	}

	// Creo o edito un registro
	public function Set($datos=false){
		// Si viene Id edito sino creo
		if(isset($datos["id"])){
			$sSql = "UPDATE ".self::$tablaClientes." SET ";
			$sSqlWhere = " WHERE id='".$datos["id"]."'";
			$nId = $datos["id"];
		} else {
			$sSql = "INSERT INTO ".self::$tablaClientes." SET ";
			$sSqlWhere = "";
			$nId = null;
		}
		foreach ($datos as $indice => $valor){ 
			if($valor === "NULL"){
				$sSql .= $indice."=".$valor.","; 
			}else{
				$sSql .= $indice."='".$valor."',"; 
			}
		}
		$sSql = substr ($sSql, 0, strlen($sSql) - 1).$sSqlWhere;
		if($this->oDb->execute($sSql)){
			if(!$nId){ $nId = $this->oDb->insert_ID(); }
			return $nId;
		}else{
			return false;
		}
	}

	// Elimino un registro
	public function Delete($nId = false){
		if(!$nId){  WriteLog("El id no es un número"); return false;  }
		$sSql = "DELETE FROM ".self::$tablaClientes." WHERE id = '".$nId."'";
		if($this->oDb->execute($sSql)){
			return true;
		}else{
			return false;
		}
	}
}?>