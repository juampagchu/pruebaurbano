<?php
/**
* cClientesGrupos.php 
*/
// Manejo de base de datos
require_once '../db.php';

class cClientesGrupos extends cDb {
	static private $tablaClientesGrupo = NULL;
		
	public function __construct(){
		// le asigno el nombre de la constante
		self::$tablaClientesGrupo = _NDB_grupos_clientes;
		$this->conectar();
	}

	// Obtiene todos los registros o bien los filtra
	public function GetAll($busqueda = ""){
		$sSql = "SELECT * FROM ".self::$tablaClientesGrupo." WHERE 1 ";
		if(!empty($busqueda)){
			$busqueda = strtolower($busqueda);
			$sSql .= " AND LOWER(c.nombre) LIKE '%".$busqueda."%' ";
		}
		$resp = $this->oDb->getArray($sSql);
		if(!empty($resp)){
			return $resp;
		}else{
			return false;
		}
	}
	
	// Obtiene un registro por ID
	public function Get($nId = false){
		if(!$nId){  WriteLog("El id no es un número"); return false;  }
		$sSql = "SELECT * FROM ".self::$tablaClientesGrupo." WHERE id = '".$nId."'";
		$resp = $this->oDb->getArray($sSql);
		if(!empty($resp) && isset($resp[0])){
			return $resp[0];
		}else{
			return false;
		}
	}

	// Creo o edito un registro
	public function Set($datos=false){
		// Si viene Id edito sino creo
		if(isset($datos["id"])){
			$sSql = "UPDATE ".self::$tablaClientesGrupo." SET ";
			$sSqlWhere = " WHERE id='".$datos["id"]."'";
			$nId = $datos["id"];
		} else {
			$sSql = "INSERT INTO ".self::$tablaClientesGrupo." SET ";
			$sSqlWhere = "";
			$nId = null;
		}
		foreach ($datos as $indice => $valor){ $sSql .= $indice."='".$valor."',"; }
		$sSql = substr ($sSql, 0, strlen($sSql) - 1).$sSqlWhere;
		if($this->oDb->execute($sSql)){
			if(!$nId){ $nId = $this->oDb->insert_ID(); }
			return $nId;
		}else{
			WriteLog("SQL que fallo:: ".$sSql);
			return false;
		}
	}

	// Elimino un registro
	public function Delete($nId = false){
		if(!$nId){  WriteLog("El id no es un número"); return false;  }
		$sSql = "DELETE FROM ".self::$tablaClientesGrupo." WHERE id = '".$nId."'";
		if($this->oDb->execute($sSql)){
			return true;
		}else{
			WriteLog("SQL que fallo:: ".$sSql);
			return false;
		}
	}
}?>