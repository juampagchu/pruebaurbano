<?php
// Conexión a la base de datos
// Cargo los nombres de las tablas
require_once 'config_datatables.php';
class cDb{
	public $oDb = NULL;
	
	function conectar(){
		global $ADODB_FETCH_MODE;
		$result = null;
		// Parar organizarme armo un array con los datos de base de datos
		$configDb = array(
			'host' => ((!empty(_DB_HOST))? _DB_HOST:''),
			'user' => ((!empty(_DB_USER))? _DB_USER:''),
			'pass' => ((!empty(_DB_PASSWORD))? _DB_PASSWORD:''),
			'database' => ((!empty(_DB_DATABASE))? _DB_DATABASE:'')
		);
		// Contemplo la conexion a base de datos con PHP 7 o superior
		if(version_compare(phpversion(), '7.0.0', '>')  == true){
			require_once "../3rdparty/adodb_lite_7/adodb.inc.php";
			$db = adoNewConnection("mysqli");
			$db->debug = false;
			$db->last_module_name = 'mysqlt_driver';
			$db->raiseErrorFn = (defined('ADODB_ERROR_HANDLER')) ? ADODB_ERROR_HANDLER : false;
			$db->setFetchMode(ADODB_FETCH_ASSOC);  
			$db->query_count = 0;
			$db->query_time_total = 0;
			$result = $db->_connect($configDb['host'], $configDb['user'], $configDb['pass'], $configDb['database'], false, false);
		}else{
			// Contemplo la conexion a base de datos con PHP 5
			define('ADODB_DIR', dirname(__FILE__)."/3rdparty/");
			require_once ADODB_DIR."adodb_lite_5/adodb.inc.php";
			include_once ADODB_DIR.'adodb_lite_5/adodbSQL_drivers/mysql/mysql_driver.inc';
			$db = new mysql_driver_ADOConnection();
			$db->debug = false;
			$db->last_module_name = 'mysql_driver';
			$db->raiseErrorFn = (defined('ADODB_ERROR_HANDLER')) ? ADODB_ERROR_HANDLER : false;
			//$db->setFetchMode(ADODB_FETCH_ASSOC);  
			$db->query_count = 0;
			$db->query_time_total = 0;
			$result = $db->_connect($configDb['host'], $configDb['user'], $configDb['pass'], $configDb['database'], false, false);
		}
		if (!$result) { echo("no se pudo conectar a la base de datos"); return false; }
		$this->oDb = $db;
		return $db;
	}
}
?>
