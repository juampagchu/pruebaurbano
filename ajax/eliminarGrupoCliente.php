<?php
	/*
		Juampa
		27/08/2021
		Elimina un grupo de cliente, verificando si el ID es verdadero.
	*/
	// Integro todo lo que voy a utilizar 
  	require_once '../tools.php';
  	require_once '../libs/gurposClientes.php';
  
	// Coloco en el log lo que viene
	WriteLog("eliminar grupo de cliente Datos ".print_r($_GET,true));

	// Instancio los modelos
	$cClientesGrupos = new cClientesGrupos();
  	$id = null;
  	$cliente = null;
  	// Verifico que el id venga
  	if(!empty($_GET["id"])){
	    // Compruebo el id
	    $id = trim($_GET["id"]);
	    $clientesGrupos = $cClientesGrupos->Get($id);
    	if(empty($clientesGrupos)){
      		WriteLog("El id es invalido no existen datos con este id.");
      		$error["id"] = "exist";
    	}
  	}else{
    	$error["id"] = "format";
    	WriteLog("El id no es un número");
  	}
  	// Verifico que tenga errores
  	if(!empty($error)){
    	// En el caso que los tenga freno el proceso y los vuelco en la respuesta
    	ResponseError($error);
    	return false;
  	}
  	if($cClientesGrupos->Delete($id)){
  		// Escribo que se guardo el log
		WriteLog("Se elimino el registro correctamente con el ID ");
		// Respondo que se elimino
		ResponseOk(array("ok"=>"ok")); return;
  	}else{
  		// Informo el erro
  		ResponseError(array("guardado"=>"No se pudo eliminar"));
		WriteLog("No se pudo eliminar."); return;
  	}
?>