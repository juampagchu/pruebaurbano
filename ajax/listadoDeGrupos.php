<?php
	/*
		Juampa
		27/08/2021
		Lista los clientes guardados en base de datos
	*/
	// Integro todo lo que voy a utilizar 
	require_once '../tools.php';
	require_once '../libs/gurposClientes.php';
	// Instancio el modelo de grupo de clientes
	$cClientesGrupos = new cClientesGrupos();
	// Consulto sin parametro para que vengan todos
	$clientesGrupos = $cClientesGrupos->GetAll();
	// Respondo
	ResponseOk($clientesGrupos);
?>