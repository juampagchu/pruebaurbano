<?php
	/*
		Juampa
		27/08/2021
		Elimina un cliente, verificando si el id es verdadero.
	*/
	// Integro todo lo que voy a utilizar 
  	require_once '../tools.php';
  	require_once '../libs/clientes.php';
  
	// Coloco en el log lo que viene
	WriteLog("eliminar cliente Datos ".print_r($_GET,true));

	// Instancio los modelos
	$cClientes = new cClientes();
  	$id = null;
  	$cliente = null;
  	// Verifico que el id venga
  	if(!empty($_GET["id"])){
	    // Compruebo el id
	    $id = trim($_GET["id"]);
	    $cliente = $cClientes->Get($id);
    	if(empty($cliente)){
      		WriteLog("El id es invalido no existen datos con este id.");
      		$error["id"] = "exist";
    	}
  	}else{
    	$error["id"] = "format";
    	WriteLog("El id no es un número");
  	}
  	// Verifico que tenga errores
  	if(!empty($error)){
    	// En el caso que los tenga freno el proceso y los vuelco en la respuesta
    	ResponseError($error);
    	return false;
  	}
  	if($cClientes->Delete($id)){
  		// Escribo que se guardo el log
		WriteLog("Se elimino el registro correctamente con el ID ");
		// Respondo que se elimino
		ResponseOk(array("ok"=>"ok")); return;
  	}else{
  		// Informo el erro
  		ResponseError(array("guardado"=>"No se pudo eliminar"));
		WriteLog("No se pudo eliminar."); return;
  	}
?>