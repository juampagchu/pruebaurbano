<?php
	/*
		Juampa
		27/08/2021
		Editar un grupo de cliente, verificando y validando la información recibida.
	*/
	// Integro todo lo que voy a utilizar 
  	require_once '../tools.php';
  	require_once '../libs/gurposClientes.php';
  
	// Coloco en el log lo que viene
	WriteLog("eliminar grupo de cliente Datos ".print_r($_GET,true));

	// Instancio los modelos
	$cClientesGrupos = new cClientesGrupos();
  	$id = null;
  	$cliente = null;
  	// Verifico que el id venga
  	if(!empty($_GET["id"])){
	    // Compruebo el id
	    $id = trim($_GET["id"]);
	    $clientesGrupos = $cClientesGrupos->Get($id);
    	if(empty($clientesGrupos)){
      		WriteLog("El id es invalido no existen datos con este id.");
      		$error["id"] = "exist";
    	}
  	}else{
    	$error["id"] = "format";
    	WriteLog("El id no es un número");
  	}
  	// Verifico que tenga errores
  	if(!empty($error)){
    	// En el caso que los tenga freno el proceso y los vuelco en la respuesta
    	ResponseError($error);
    	return false;
  	}
?>
  <div class="card">
    <div class="card-body">
      <h3 class="card-title">Crear grupo de clientes</h3>
    </div>
    <div class="card-body">
        <form id="frm-Cliente" action="#">
          <div class="card-body">
            <input type="hidden" id="id" value="<?php echo($clientesGrupos["id"]);?>">
            <div class="form-group">
              <label>Nombre (*)</label>
              <input type="text" class="form-control" id="nombre" placeholder="Nombre" value="<?php echo($clientesGrupos["nombre"]);?>">
              <span class="error invalid-feedback d-none">Revisa esté campo</span>
            </div>
            <div class="form-group pt-5">
              <button type="button" onclick="CancelarCrearGrupoCliente();" class="btn btn-warning">Cancelar</button>
              <button type="button" onclick="GuardarGrupoCliente();" class="btn btn-primary float-right">Guardar</button>
            </div>
        </form>
    </div>
  </div>