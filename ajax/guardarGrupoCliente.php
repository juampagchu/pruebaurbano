<?php
	/*
		Juampa
		27/08/2021
		Crea un nuevo grupo de cliente, verificando los datos recibidos.
	*/
	// Integro todo lo que voy a utilizar 
	require_once '../tools.php';
	require_once '../libs/gurposClientes.php';

	// Instancio los modelos
	$cClientesGrupos = new cClientesGrupos();
	
	// Predifino una variable de manejo de errores
	$error = array();
	// Predifino el id cliente
	$id = null;
	// Coloco en el log lo que viene
	WriteLog("guardarCliente Datos ".print_r($_POST,true));
	// Verifico que el id venga
	if(isset($_POST["id"])){
		// Compruebo el id
		$id = trim($_POST["id"]);
		if(empty($id)){
			$error["id"] = "format";
			WriteLog("El id no es un número");
		}else{
			$clientesGrupos = $cClientesGrupos->Get($id);
			if(empty($clientesGrupos)){
				WriteLog("El id es invalido no existen datos con este id.");
				$error["id"] = "exist";
			}
		}
	}
	// Verifico que el nombre no este vacio
	if(empty(trim($_POST["nombre"]))){
		$error["nombre"] = "empty";
		WriteLog("El nombre esta vacio");
	}
	// El estado es HAB por defecto
	$estado = "HAB";
	// Verifico que el estado exista
	if(isset($_POST["estado"])){
		// Verifico que el estado sea valido
		if (!in_array($_POST["estado"], ['HAB','DES','ELI'])) {
			$error["estado"] = "format";
			WriteLog("El estado ".$_POST["estado"]." no es un dato aceptado");
		}else{
			// Si existe el estado lo coloco en la variable
			$estado = $_POST["estado"];
		}
	}

	// Verifico que tenga errores
	if(!empty($error)){
		// En el caso que los tenga freno el proceso y los vuelco en la respuesta
		WriteLog("Paro el proceso de guardo por tener errores.");
		ResponseError($error);
		return false;
	}
	// Armo el registro de datos del cliente
	$registro = array(
		"nombre"=>$_POST["nombre"],
		"estado"=>$estado
	);
	// Si existe un id es porque esta editando el cliente
	if(!empty($id)){
		$registro["id"] = $id;
	}else{
		// Sino es porque lo estan creando y agrego la fecha de creación
		$registro["creacion"] = date("Y-m-d H:i:s");
	}
	// Coloco el log como queda formado todo
	WriteLog("Me queda formado el registro asi: ".print_r($registro,true));
	// Mando a guardar en la base de datos
	$idSet = $cClientesGrupos->Set($registro);
	// Verifico que se guarde todo correctamente
	if(!$idSet){
		ResponseError(array("guardado"=>"No se pudo guardar"));
		WriteLog("No se guardo el registro."); return;
	}
	// Escribo que se guardo el log
	WriteLog("Se guardo el registro correctamente con el ID ".$idSet);
	// Respondo con el id generado
	ResponseOk(array("id"=>$idSet)); return;
?>