<?php
	/*
		Juampa
		27/08/2021
		Crea un nuevo cliente, verificando los datos recibidos.
	*/
	// Integro todo lo que voy a utilizar 
	require_once '../tools.php';
	require_once '../libs/clientes.php';
	require_once '../libs/gurposClientes.php';

	// Instancio los modelos
	$cClientes = new cClientes();
	$cClientesGrupos = new cClientesGrupos();

	// Dejo una expRegular para verificar email
	$rxemail = "/^(?:[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/iD";
	// Predifino una variable de manejo de errores
	$error = array();
	// Predifino el id cliente
	$id = null;
	// Coloco en el log lo que viene
	WriteLog("guardarCliente Datos ".print_r($_POST,true));
	// Verifico que el id venga
	if(isset($_POST["id"])){
		// Compruebo el id
		$id = trim($_POST["id"]);
		if(empty($id)){
			$error["id"] = "format";
			WriteLog("El id no es un número");
		}else{
			$clienteEditado = $cClientes->Get($id);
			if(empty($clienteEditado)){
				WriteLog("El id es invalido no existen datos con este id.");
				$error["id"] = "exist";
			}
		}
	}
	// Predifino el id grupo cliente
	$grupoId = "NULL";
	if(isset($_POST["grupo_id"])){
		// Si no esta vacio lo verifico
		if(!empty(trim($_POST["grupo_id"]))){
			$grupoId = trim($_POST["grupo_id"]);
			if(empty($grupoId)){
				$error["grupo_id"] = "format";
				WriteLog("El grupo id no es un número");
			}else{
				$grupoDeClientes = $cClientesGrupos->Get($grupoId);
				if(empty($grupoDeClientes)){
					$error["grupo_id"] = "exist";
					WriteLog("El grupo id es invalido no existen datos con este id.");
				}
			}
		}
	}

	// Verifico que el nombre no este vacio
	if(empty(trim($_POST["nombre"]))){
		$error["nombre"] = "empty";
		WriteLog("El nombre esta vacio");
	}
	// Verifico que el apellido no este vacio
	if(empty(trim($_POST["apellido"]))){
		$error["apellido"] = "empty";
		WriteLog("El apellido esta vacio");
	}
	// Verifico que el email no este vacio
	if(empty(trim($_POST["email"]))){
		$error["email"] = "empty";
		WriteLog("El email esta vacio");
	}else{
		// Verifico que el email sea valido
		if (!preg_match($rxemail, $_POST["email"])) {
			$error["email"] = "format";
			WriteLog("El email esta mal formado");
		}
	}
	// El estado es HAB por defecto
	$estado = "HAB";
	// Verifico que el estado exista
	if(isset($_POST["estado"])){
		// Verifico que el estado sea valido
		if (!in_array($_POST["estado"], ['HAB','DES','ELI'])) {
			$error["estado"] = "format";
			WriteLog("El estado ".$_POST["estado"]." no es un dato aceptado");
		}else{
			// Si existe el estado lo coloco en la variable
			$estado = $_POST["estado"];
		}
	}

	// Verifico que tenga errores
	if(!empty($error)){
		// En el caso que los tenga freno el proceso y los vuelco en la respuesta
		WriteLog("Paro el proceso de guardo por tener errores.");
		ResponseError($error);
		return false;
	}
	// Armo el registro de datos del cliente
	$registro = array(
		"nombre"=>$_POST["nombre"],
		"apellido"=>$_POST["apellido"],
		"email"=>$_POST["email"],
		"estado"=>$estado,
		"grupo_id"=>$grupoId,
		"observaciones"=>$_POST["observaciones"]
	);
	// Si existe un id es porque esta editando el cliente
	if(!empty($id)){
		$registro["id"] = $id;
	}else{
		// Sino es porque lo estan creando y agrego la fecha de creación
		$registro["creacion"] = date("Y-m-d H:i:s");
	}
	// Coloco el log como queda formado todo
	WriteLog("Me queda formado el registro asi: ".print_r($registro,true));
	// Mando a guardar en la base de datos
	$idSet = $cClientes->Set($registro);
	// Verifico que se guarde todo correctamente
	if(!$idSet){
		ResponseError(array("guardado"=>"No se pudo guardar"));
		WriteLog("No se guardo el registro."); return;
	}
	// Escribo que se guardo el log
	WriteLog("Se guardo el registro correctamente con el ID ".$idSet);
	// Respondo con el id generado
	ResponseOk(array("id"=>$idSet)); return;
?>