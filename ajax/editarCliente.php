<?php
	/*
		Juampa
		27/08/2021
		Formulario para crear un cliente
	*/
  // Integro todo lo que voy a utilizar 
  require_once '../tools.php';
  require_once '../libs/clientes.php';
  require_once '../libs/gurposClientes.php';
  $cClientesGrupos = new cClientesGrupos();
  $clientesGrupos = $cClientesGrupos->GetAll($busqueda);

  // Coloco en el log lo que viene
  WriteLog("editar cliente Datos ".print_r($_GET,true));

  // Instancio los modelos
  $cClientes = new cClientes();
  $id = null;
  $cliente = null;
  // Verifico que el id venga
  if(!empty($_GET["id"])){
    // Compruebo el id
    $id = trim($_GET["id"]);
    $cliente = $cClientes->Get($id);
    if(empty($cliente)){
      WriteLog("El id es invalido no existen datos con este id.");
      $error["id"] = "exist";
    }
  }else{
    $error["id"] = "format";
    WriteLog("El id no es un número");
  }
  // Verifico que tenga errores
  if(!empty($error)){
    // En el caso que los tenga freno el proceso y los vuelco en la respuesta
    ResponseError($error);
    return false;
  }
?>
<div class="card">
    <div class="card-body">
      <h3 class="card-title">Editar clientes: <?php echo($cliente["nombre"]);?></h3>
    </div>
    <div class="card-body">
        <form id="frm-Cliente" action="#">
          <?php if(empty($clientesGrupos)){ ?>
            <div class="card text-dark bg-warning mb-3">
              <div class="card-body">
                <h5 class="card-title">Faltan grupos de clientes</h5>
                <p class="card-text">Vemos que no tiene grupos de clientes para asignarle a este cliente. Podra crear de igual forma este cliente y luego asignarle el grupo de cliente o bien crear un grupo de cliente presionando el botón de abajo.</p>
                <button type="button" onclick="CrearGrupoPersonasForce();" class="btn btn-primary float-right">Crear grupo</button>
              </div>
            </div>
          <?php } ?>
          <div class="card-body">
            <input type="hidden" id="idCliente" value="<?php echo($cliente["id"]);?>">
            <div class="form-group">
              <label>Nombre (*)</label>
              <input type="text" class="form-control" id="nombre" value="<?php echo($cliente["nombre"]);?>" placeholder="Nombre">
              <span class="error invalid-feedback d-none">Revisa esté campo</span>
            </div>
            <div class="form-group">
              <label>Apellido (*)</label>
              <input type="text" class="form-control" id="apellido" value="<?php echo($cliente["apellido"]);?>" placeholder="Apellido">
              <span class="error invalid-feedback d-none">Revisa esté campo</span>
            </div>
            <div class="form-group">
              <label>Email (*)</label>
              <input type="email" class="form-control" id="email" value="<?php echo($cliente["email"]);?>" placeholder="Email">
              <span class="error invalid-feedback d-none">Revisa esté campo</span>
            </div>
            <div class="form-group">
              <label>Estado</label>
              <select class="form-control" id="estado">
                <option <?php echo((("HAB" == $cliente["estado"])? "selected":"")); ?> value="HAB">Habilitado</option>
                <option <?php echo((("DES" == $cliente["estado"])? "selected":"")); ?> value="DES">Deshabilitado</option>
                <option <?php echo((("ELI" == $cliente["estado"])? "selected":"")); ?> value="ELI">Eliminado</option>
              </select>
            </div>
            <div class="form-group">
              <label>Grupo de clientes</label>
              <select class="form-control" id="grupo_id">
                <option value="">Sin definir</option>
                <?php if(!empty($clientesGrupos)){ ?>
                  <?php foreach($clientesGrupos as $cg){ ?>
                    <option <?php echo((($cg["id"] == $cliente["grupo_id"])? "selected":"")); ?> value="<?php echo($cg["id"]); ?>"><?php echo($cg["nombre"]); ?></option>
                  <?php } ?>
                <?php } ?>
              </select>
            </div>
            <div class="form-group">
              <label>Observaciones</label>
              <textarea class="form-control" id="observaciones" placeholder="Observaciones"><?php echo($cliente["observaciones"]);?></textarea>
            </div>
            <div class="form-group pt-5">
              <button type="button" onclick="CancelarCrearCliente();" class="btn btn-warning">Cancelar</button>
              <button type="button" onclick="GuardarCliente();" class="btn btn-primary float-right">Guardar</button>
            </div>
        </form>
    </div>
  </div>