<?php
	/*
		Juampa
		27/08/2021
		Formulario para crear un cliente
	*/
?>
  <div class="card">
    <div class="card-body">
      <h3 class="card-title">Crear grupo de clientes</h3>
    </div>
    <div class="card-body">
        <form id="frm-Cliente" action="#">
          <div class="card-body">
            <div class="form-group">
              <label>Nombre (*)</label>
              <input type="text" class="form-control" id="nombre" placeholder="Nombre">
              <span class="error invalid-feedback d-none">Revisa esté campo</span>
            </div>
            <div class="form-group pt-5">
              <button type="button" onclick="CancelarCrearGrupoCliente();" class="btn btn-warning">Cancelar</button>
              <button type="button" onclick="GuardarGrupoCliente();" class="btn btn-primary float-right">Guardar</button>
            </div>
        </form>
    </div>
  </div>