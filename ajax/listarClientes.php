<?php
	/*
		Juampa
		27/08/2021
		Lista los clientes guardados en base de datos
	*/
	// Integro todo lo que voy a utilizar 
	require_once '../tools.php';
	require_once '../libs/clientes.php';
	// Instancio el modelo de clientes
	$cClientes = new cClientes();
	// Verifico si viene algo en busqueda
	$busqueda = ((!empty(@$_GET["busqueda"]))? $_GET["busqueda"]:"");
	// Verifico si viene un grupo id
	$grupoId = ((!empty(@$_GET["grupo_id"]))? $_GET["grupo_id"]:"");
	// Voy a buscar los de clientes
	$clientes = $cClientes->GetAll($busqueda,$grupoId);
	// Verifico que vengan datos
	if(!empty($clientes)){
		// Si tiene datos los voy pintando
		foreach($clientes as $cliente){
?>
		<tr>

		  <td scope="col"><?php echo($cliente["nombre"]);?></td>
		  <td scope="col"><?php echo($cliente["apellido"]);?></td>
		  <td scope="col"><?php echo($cliente["email"]);?></td>
		  <td scope="col"><?php echo($cliente["nombre_grupo"]);?></td>
		  <td scope="col text-right">
		    <button type="button" onclick="EditarCliente('<?php echo($cliente["id"]);?>')" class="btn btn-primary btn-xs float-right">Editar</button>
		    <button type="button" onclick="EliminarCliente('<?php echo($cliente["id"]);?>')" class="btn btn-danger btn-xs float-right mr-1">Eliminar</button>
		  </td>
		</tr>
<?php		
		}
	}else{
		// Si no tiene datos muestro un mensaje
?>
	<tr>
		<td class="text-center" colspan="5">No se encontraron clientes.</td>
	</tr>
<?php		
	}
?>