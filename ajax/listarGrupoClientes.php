<?php
	/*
		Juampa
		27/08/2021
		Lista los clientes guardados en base de datos
	*/
	// Integro todo lo que voy a utilizar 
	require_once '../tools.php';
	require_once '../libs/gurposClientes.php';
	// Instancio el modelo de clientes grupos
	$cClientesGrupos = new cClientesGrupos();
	// Verifico si viene algo en busqueda
	$busqueda = ((!empty(@$_GET["busqueda"]))? $_GET["busqueda"]:"");
	// Voy a buscar los grupos de clientes
	$clientesGrupos = $cClientesGrupos->GetAll($busqueda);
	// Verifico que vengan datos
	if(!empty($clientesGrupos)){
		// Si tiene datos los voy pintando
		foreach($clientesGrupos as $clienteGrupo){
?>
		<tr>

		  <td scope="col"><?php echo($clienteGrupo["nombre"]);?></td>
		  <td scope="col text-right">
		    <button type="button" onclick="EditarGrupoCliente('<?php echo($clienteGrupo["id"]);?>')" class="btn btn-primary btn-xs float-right">Editar</button>
		    <button type="button" onclick="EliminarGrupoCliente('<?php echo($clienteGrupo["id"]);?>')" class="btn btn-danger btn-xs float-right mr-1">Eliminar</button>
		  </td>
		</tr>
<?php		
		}
	}else{
		// Si no tiene datos muestro un mensaje
?>
	<tr>
		<td class="text-center" colspan="2">No se encontraron grupos de clientes.</td>
	</tr>
<?php		
	}
?>