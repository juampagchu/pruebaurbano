<?php
	/*
		Juampa
		27/08/2021
		Formulario para crear un cliente
	*/
  require_once '../libs/gurposClientes.php';
  $cClientesGrupos = new cClientesGrupos();
  $clientesGrupos = $cClientesGrupos->GetAll($busqueda);
?>
  <div class="card">
    <div class="card-body">
      <h3 class="card-title">Crear clientes</h3>
    </div>
    <div class="card-body">
        <form id="frm-Cliente" action="#">
          <?php if(empty($clientesGrupos)){ ?>
            <div class="card text-dark bg-warning mb-3">
              <div class="card-body">
                <h5 class="card-title">Faltan grupos de clientes</h5>
                <p class="card-text">Vemos que no tiene grupos de clientes para asignarle a este cliente. Podra crear de igual forma este cliente y luego asignarle el grupo de cliente o bien crear un grupo de cliente presionando el botón de abajo.</p>
                <button type="button" onclick="CrearGrupoPersonasForce();" class="btn btn-primary float-right">Crear grupo</button>
              </div>
            </div>
          <?php } ?>
          <div class="card-body">
            <div class="form-group">
              <label>Nombre (*)</label>
              <input type="text" class="form-control" id="nombre" placeholder="Nombre">
              <span class="error invalid-feedback d-none">Revisa esté campo</span>
            </div>
            <div class="form-group">
              <label>Apellido (*)</label>
              <input type="text" class="form-control" id="apellido" placeholder="Apellido">
              <span class="error invalid-feedback d-none">Revisa esté campo</span>
            </div>
            <div class="form-group">
              <label>Email (*)</label>
              <input type="email" class="form-control" id="email" placeholder="Email">
              <span class="error invalid-feedback d-none">Revisa esté campo</span>
            </div>
            <div class="form-group">
              <label>Grupo de clientes</label>
              <select class="form-control" id="grupo_id">
                <option value="">Sin definir</option>
                <?php if(!empty($clientesGrupos)){ ?>
                  <?php foreach($clientesGrupos as $cg){ ?>
                    <option value="<?php echo($cg["id"]); ?>"><?php echo($cg["nombre"]); ?></option>
                  <?php } ?>
                <?php } ?>
              </select>
            </div>
            <div class="form-group">
              <label>Observaciones</label>
              <textarea class="form-control" id="observaciones" placeholder="Observaciones"></textarea>
            </div>
            <div class="form-group pt-5">
              <button type="button" onclick="CancelarCrearCliente();" class="btn btn-warning">Cancelar</button>
              <button type="button" onclick="GuardarCliente();" class="btn btn-primary float-right">Guardar</button>
            </div>
        </form>
    </div>
  </div>